# srrg_scan_matcher

This package contains the basic functions and utilities for scan matching and 2D pose tracking.

## Prerequisites

This package requires:
* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules)
* [srrg_boss](https://gitlab.com/srrg-software/srrg_boss)
* [srrg_core](https://gitlab.com/srrg-software/srrg_core)

