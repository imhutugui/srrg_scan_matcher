#include <cstdio>
#include <fstream>

#include "tracker2d.h"
#include "projective_correspondence_finder2d.h"
#include "nn_correspondence_finder2d.h"

namespace srrg_scan_matcher {
  using namespace std;

  inline double tv2sec(struct timeval& tv) {
    return (double) tv.tv_sec + 1e-6 * (double) tv.tv_usec;
  }

  Tracker2D::~Tracker2D() {
  }

  //! returns the system time in seconds
  double getTime() {
    struct timeval tv;
    gettimeofday(&tv, 0);
    return tv2sec(tv);
  }

  Tracker2D::Tracker2D(Projector2D* _projector) {
    _iterations = 30;
    _bpr = 0.05;
    _global_t.setIdentity();
    _last_clipped_pose.setIdentity();
    _min_correspondences_ratio = 0.5;
    _local_map_clipping_range = 5;
    _clip_translation_threshold = 5;
    _dump_filename="";
    _dump_stream=0;
    setReference(0);
    setCurrent(0);
    _voxelize_resolution=0;
    _correspondence_finder=0;
    setProjector(_projector);
    _verbose=false;
    _status=OK;
  }

  
  void Tracker2D::setProjector(Projector2D* projector_) { 
    ProjectiveCorrespondenceFinder2D *pcorr= dynamic_cast<ProjectiveCorrespondenceFinder2D*>(correspondenceFinder());
    if (pcorr) {
      cerr << "Projector set in finder" << pcorr << endl;
      pcorr->setProjector(projector_);
    }
    cerr << "Projector set in validator" << projector_ << endl;
    ProjectiveValidator2D::setProjector(projector_);
    if (projector_) {
      Projector2D* merger_projector=new Projector2D(*projector_);
      Merger2D::setProjector(merger_projector);
    }
  }


  void Tracker2D::update(Cloud2D* cloud_, 
			 const Eigen::Isometry2f& prior_mean,
			 const Eigen::Matrix3f& prior_omega) {

    if (! cloud_) {
      return;
    }
    
    if (!_reference) {
      setReference(cloud_);
      setCurrent(0);
      _status=FirstScan;
      return;
    } else {
      if (_current && _current!=_reference)
	delete _current;
      setCurrent(cloud_);
      _current->setColor(Eigen::Vector3f(0,1,0));
    }
    
    if (! _current || ! _reference){
      _status=Error;
      return;
    }
    double init = getTime();

    _reference->setColor(Eigen::Vector3f(1,0,0));
    Aligner2D::align(prior_mean, prior_omega);
    if (srrg_core::isNan(solver()->T().matrix())) {
      cerr << "nan" << endl;
      reset(_global_t*prior_mean);
      _last_clipped_pose = _global_t;
      _status=SolverNAN;
      throw std::runtime_error("nonononono");
      return;
    }


    Cloud2D temp_current(*_current);
    temp_current.transformInPlace(_solver->T());
    ProjectiveValidator2D::setCurrent(&temp_current);
    ProjectiveValidator2D::compute();
    int num_correspondences=Validator2D::numCorrespondences();
    float correspondences_ratio = (float) num_correspondences/(float) temp_current.size();
    if (!num_correspondences ||  correspondences_ratio < _min_correspondences_ratio) {
      cerr << "nc: " << num_correspondences << " " << correspondences_ratio << endl;
      reset(_global_t*prior_mean);
      _status=BadCorrespondences;
      return;
    }

    if (Validator2D::badPointsRatio() > _bpr){
      cerr << "bpr: " << Validator2D::badPointsRatio() << endl;
      reset(_global_t*prior_mean);
      _status=BadBPR;
      return;
    }

    Merger2D::compute( *_reference, temp_current, _solver->T().inverse());

    reference()->transformInPlace(_solver->T().inverse());

    if (_voxelize_resolution>0) {
      CloudProcessor::voxelize(*_reference,_voxelize_resolution);
    }

    _global_t = _global_t * _solver->T();
    
    Eigen::Isometry2f delta_clip = _last_clipped_pose.inverse() * _global_t;
    if (delta_clip.translation().norm() > _clip_translation_threshold) {
      dump();
      reference()->clip(_local_map_clipping_range);
      _last_clipped_pose=_global_t;		
      std::cerr << "Clipping" << std::endl;
    }
      
    double finish = getTime() - init;
    _cycle_time = finish;
    if (_verbose){
      std::cerr << "Max freq: " << 1.0/_cycle_time;
      std::cerr << ". Num correspondences: " << num_correspondences;
      std::cerr << ". Bpr: " << Validator2D::badPointsRatio();
      std::cerr << ". Inliers: " << Validator2D::numInliers() << std::endl;
    }
    _status=OK;
  }

  void Tracker2D::reset(const Eigen::Isometry2f& global_t_) {
    cerr << "reset!" << endl;
    _global_t=global_t_;
    _last_clipped_pose=_global_t;
    dump();
    if (_reference ){
      delete _reference;
      setReference(0);
    }

    if (_current) {
      setReference(_current);
      setCurrent(0);
    }
  }

  void Tracker2D::setDumpFilename(const std::string& dump_filename_) {
    if (_dump_stream){
      _dump_stream->close();
      delete _dump_stream;
      _dump_stream=0;
    }
    if (dump_filename_.length()) {
      _dump_stream=new ofstream(dump_filename_.c_str());
      _dump_filename=dump_filename_;
    }
    _frame_count=0;
  }

  void Tracker2D::dump(){
    if (! _dump_stream)
      return;
    if (!reference())
      return;
    char current_dump[1024];
    sprintf(current_dump, "%s-%05d.dat", _dump_filename.c_str(), _frame_count);
    * (_dump_stream) << _frame_count << " ";
    Eigen::Vector3f v=srrg_core::t2v(_global_t);
    * (_dump_stream) << v.transpose() << " ";
    * (_dump_stream) << current_dump << endl;
    ofstream os(current_dump);
    reference()->save(os);
    _frame_count++;
  }

  void Tracker2D::clear(){
    _global_t.setIdentity();
    _last_clipped_pose.setIdentity();
    delete _reference;
    delete _current;
    setReference(0);
    setCurrent(0);
    _status=OK;
  }
		     
}
