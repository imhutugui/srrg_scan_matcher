#include "validator2d.h"

namespace srrg_scan_matcher {

  Validator2D::Validator2D() {
    _reference=0;
    _current=0;
    _inlier_distance=0.2;
    _bad_points_ratio = -1;
    _num_outliers = 0;
    _num_inliers = 0;
    _num_correspondences = 0;
  }

  Validator2D::~Validator2D(){}

 
}
