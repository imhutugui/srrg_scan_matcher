#include "cloud_processor.h"
#include <stdexcept>
using namespace std;
namespace srrg_scan_matcher {
  using namespace srrg_core;
  struct IndexPair {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    IndexPair() {
      x = y = 0;
      index = -1;
    }
      
    // Resolution is in cm
    IndexPair(const Eigen::Vector2f& v, int idx, float ires) {
      x = static_cast<int>(ires * v.x());
      y = static_cast<int>(ires * v.y());
      index = idx;
    }

    inline bool operator<(const IndexPair& o) const {
      if (x < o.x)
	return true;
      if (x > o.x)
	return false;
      if (y < o.y)
	return true;
      if (y > o.y)
	return false;
      if (index < o.index)
	return true;

      return false;
    }

    inline bool sameCell(const IndexPair& o) const {
      return x == o.x && y == o.y;
    }

    int x, y, index;
  };


  void CloudProcessor::voxelize(Cloud2D& model, float res) {
    Cloud2D sparse_model;
    float ires = 1. / res;

    std::vector<IndexPair> voxels(model.size());

    for (int i = 0; i < model.size(); ++i){
      voxels[i] = IndexPair(model[i].point(), i , ires);
    }
    
    sparse_model.resize(model.size());
    std::sort(voxels.begin(), voxels.end());

    int k = -1;
    for (size_t i = 0; i < voxels.size(); ++i) { 
      IndexPair& pair = voxels[i];
      int idx = pair.index;

      if (k >= 0 && voxels[i].sameCell(voxels[i-1])) {
	sparse_model[k] += model[idx];
      } else {
	sparse_model[++k] = model[idx];
      } 
    }

    sparse_model.resize(k);

    for (size_t i = 0; i < sparse_model.size(); ++i) {
      if (sparse_model[i].accumulator() <= 0)
	throw std::runtime_error("Negative Point Accumulator");

      sparse_model[i].normalize();
    }

    model = sparse_model;
  }

  void CloudProcessor::merge(FloatVector& dest_ranges, IntVector& dest_indices, Cloud2D& dest,
			     FloatVector& src_ranges, IntVector& src_indices, Cloud2D& src,
			     float normal_threshold, float distance_threshold) {
    int new_points = 0;
    normal_threshold = cos(normal_threshold);

    std::vector<int> indices_to_remove;
    for (size_t c = 0; c < std::min(dest_ranges.size(), src_ranges.size()); ++c) {
      int& src_idx = src_indices[c];
      int& dest_idx = dest_indices[c];

      if (dest_idx<0){
	if (src_idx > -1) {
	  new_points++;
	}
	continue;
      }
      if (src_idx<0){
	indices_to_remove.push_back(dest_idx);
	dest_idx = -1;
	continue;
      }

      float dest_depth = dest_ranges[c];
      float src_depth = src_ranges[c];
      float distance = (dest_depth - src_depth); // / 0.5 * (destDepth + srcDepth);

      // if a new point appears a lot in front an old one add it
      if (distance > distance_threshold) {
	// dest point does not need to be renormalized, since we don't touch it
	dest_idx = -1;
	++new_points;
	continue;
      }

      // if a new point appears a lot behind an old replace the old
      if (distance < -distance_threshold) {
	dest[dest_idx] = src[src_idx];
	// invalidate both indices in order to do not consider src point later
	// and do not normalize dest point (we don't need it)
	src_idx = dest_idx = -1;
	continue;
      }

      // if the normals do not match do nothing
      Eigen::Vector2f dest_normal = dest[dest_idx].normal();
      Eigen::Vector2f src_normal = src[src_idx].normal();

      
      if (!dest[dest_idx].isNormalized())
	dest_normal /= dest[dest_idx].accumulator();

      if (!src[src_idx].isNormalized())
	src_normal /= src[src_idx].accumulator();

      if (dest_normal.dot(src_normal) < normal_threshold) {
	if (!dest_normal.x() && !dest_normal.y()){
	  dest[dest_idx] = src[src_idx];
	}
	// if normals are not good just do nothing and skip the points
	dest_idx = src_idx = -1; 
	continue;
      }

      // otherwise merge the points
      dest[dest_idx] += src[src_idx];
      src_idx = -1;
    }

    // finally recompute all the accumulated points
    for (size_t c = 0; c < dest_ranges.size(); ++c) {
      int& dest_idx = dest_indices[c];

      if (dest_idx < 0)
	continue;

      dest[dest_idx].normalize();
    }

    // erase infinity points
    std::sort (indices_to_remove.begin(), indices_to_remove.end());
    for (size_t i = 0; i <indices_to_remove.size(); ++i ){
      int idx_to_remove = indices_to_remove[i];
      dest.erase(dest.begin()+idx_to_remove-i);
    }

    // and add the remaining ones from src 
    int k = dest.size();
    dest.resize(dest.size() + new_points);

    for (size_t c = 0; c < src_ranges.size(); ++c) {
      int& src_idx = src_indices[c];

      if (src_idx < 0)
	continue;

      dest[k] = src[src_idx];
      dest[k].setColor(Eigen::Vector3f(0,0,1));
      k++;
    }
  }




  //! Merges the points in dest that fall in the same radial bin
  void CloudProcessor::radialMerge(FloatVector& dest_ranges,
				   IntVector& dest_indices,
				   FloatVector& dest_reverse_ranges,
				   IntVector& dest_reverse_indices,
				   Cloud2D& dest,
				   float normal_threshold,
				   float distance_threshold) {

    std::vector<bool> suppressed(dest.size());
    normal_threshold = cos(normal_threshold);

    for (size_t i=0; i<dest.size(); i++) {
      suppressed[i]=false;
      int &bin=dest_reverse_indices[i];
      // ignore a point out of the projection
      if (bin<0)
	continue;

      // ignore a point out of range projection
      int range=dest_reverse_ranges[i];
      if (range>=1e3)
	continue;

      // ignore a point that maps in itself
      if (i==dest_indices[bin])
	continue;

      // ignore a point whose distance is too different from the one of the projected ranges
      if (fabs(range-dest_ranges[bin])>distance_threshold)
	continue;

      int dest_idx=dest_indices[bin];
      
      if (dest_idx == -1)
	continue;

      RichPoint2D& pdest=dest[dest_idx];
      RichPoint2D& psrc=dest[i];
      
      if (pdest.normal().dot(psrc.normal())<normal_threshold)
	continue;
      
      suppressed[i]=true;
      pdest+=psrc;
    }
    size_t k = 0;
    for (size_t i=0; i<dest.size(); i++){
      RichPoint2D& psrc=dest[k];
      RichPoint2D& pdest=dest[i];
      if (! suppressed[i]) {
	psrc=pdest;
	psrc.normalize();
	k++;
      }
    }
    //cerr << "radial merge, suppressed: " << dest.size() - k << endl;
    dest.resize(k);
  }



  void CloudProcessor::prune(FloatVector& dest_ranges, IntVector& dest_indices, Cloud2D& dest,
			     FloatVector& src_ranges, IntVector& src_indices, Cloud2D& src,
			     float normal_threshold, float distance_threshold) {

    
    normal_threshold = cos(normal_threshold);
    std::vector<int> indices_to_remove;
    for (size_t c = 0; c < std::min(dest_ranges.size(), src_ranges.size()); ++c) {
      int& src_idx = src_indices[c];
      int& dest_idx = dest_indices[c];

      //no point for this beam in dest cloud, do nothing
      if (dest_idx<0){
	continue;
      }
      //point in dest cloud but no on src (i.e., point has gone to infinity), mark to old to remove
      if (src_idx<0){
	indices_to_remove.push_back(dest_idx);
	dest_idx = -1;
	continue;
      }

      float dest_depth = dest_ranges[c];
      float src_depth = src_ranges[c];
      float distance = (dest_depth - src_depth);

      // if a new point appears a lot behind, mark old to remove
      if (distance < -distance_threshold) {
	indices_to_remove.push_back(dest_idx);
	// invalidate both indices in order to do not consider src point later
	// and do not normalize dest point (we don't need it)
	src_idx = dest_idx = -1;
	continue;
      }
    }

    // Finally erase infinity points
    std::sort (indices_to_remove.begin(), indices_to_remove.end());
    for (size_t i = 0; i <indices_to_remove.size(); ++i ){
      int idx_to_remove = indices_to_remove[i];
      dest.erase(dest.begin()+idx_to_remove-i);
    }
    
  }
  
}
