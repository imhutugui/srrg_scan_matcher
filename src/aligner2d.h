#pragma once
#include "solver2d.h"
#include "correspondence_finder2d.h"
#include "cloud2d.h"

namespace srrg_scan_matcher {

  /*!
     Aligner class for registering point clouds;
     It is a convenience container that is used to provide elementary icp.
     
     It owns a solver(that is destroyed when the object is deleted);
     It has a correspondence finder;


     Example initialization:

     \code{.cpp}
     Aligner2D* aligner = new Aligner2D;
     Projector2D* projector=new Projector2D;
     CorrespondenceFinder2D  cfinder = new ProjectiveCorrespondenceFinder2D;
     aligner->setSolver(new Solver2D);
     cfinder->setSolver(aligner->solver());
     cfinder->setProjector(projector);
     aligner->setCorrespondenceFinder(cfinder);
     \endcode

     the above sequence has to be done only once

     then each time you want to register two clouds
     
     ...

     \code{.cpp}
     aligner->setReference(ref);
     aligner->setCurrent(curr);
     aligner->setIterations(num_iterations);
     aligner->align();
     \endcode
     
     cerr << "transform is: " << aligner->T() << endl;
     
   */

  class Aligner2D {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    Aligner2D();
    virtual ~Aligner2D();
    // setter & getter
    inline void setIterations(const int &iterations) { _iterations = iterations; }
    inline const Eigen::Isometry2f& T() { return _solver->T(); }
    inline const Cloud2D* reference() const { return _solver->reference(); }
    inline void setReference(Cloud2D* reference_) { _solver->setReference(reference_); }
    inline const Cloud2D* current() const { return _solver->current(); }
    inline void setCurrent(Cloud2D* current) { _solver->setCurrent(current); }
    inline const Solver2D* solver() const { return _solver; }
    inline Solver2D* solver() { return _solver; }
    void setSolver(Solver2D* solver);

    inline const CorrespondenceFinder2D* correspondenceFinder() const { return _correspondence_finder;}

    inline CorrespondenceFinder2D* correspondenceFinder() { return _correspondence_finder;}
    inline void setCorrespondenceFinder(CorrespondenceFinder2D* correspondence_finder_) {
      _correspondence_finder=correspondence_finder_;
      if (_correspondence_finder)
	_correspondence_finder->setSolver(_solver);
    }

    void align(const Eigen::Isometry2f& initial_guess=Eigen::Isometry2f::Identity(), const Eigen::Matrix3f& guess_information=Eigen::Matrix3f::Zero());
  protected:
    int _iterations;			       
    Solver2D *_solver;
    CorrespondenceFinder2D* _correspondence_finder;
  };

}
