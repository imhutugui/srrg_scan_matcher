#pragma once

#include "rich_point2d.h"

namespace srrg_scan_matcher {
  typedef std::vector<RichPoint2D, Eigen::aligned_allocator<RichPoint2D> > RichPoint2DVector;

  /*!
     This class represents a 3D model, as a collection of rich points
     It provides some basic functionalities, such as serialization, clipping, transformation
     adding and it supports opengl rendering
  */
  struct Cloud2D: public RichPoint2DVector {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    //! applies the transformation to each entity in the model, doing side effect.
    //! @param dest: output
    //! @param T: the transform
    void transformInPlace(const Eigen::Isometry2f& T);

    //! applies the transformation to each entity in the model, but writes the output in dest.
    //! @param dest: output
    //! @param T: the transform
    void transform(Cloud2D& dest, const Eigen::Isometry2f& T) const;

    //! adds other to this point cloud, doing side effect
    void add(const Cloud2D& other);

    //! clips to a maxRange around a pose
    void clip(float range, const Eigen::Isometry2f& pose = Eigen::Isometry2f::Identity());

    //! lower and upper are the respectively bottom left and the top right elements
    void computeBoundingBox(Eigen::Vector2f& lower, Eigen::Vector2f& higher) const;

    //! draws the cloud in opengl with various options
    //! @param T: the transformation to apply before rendering
    //! @param draw_normals: if set draws little bars to denote the normals of the points
    //! @param use_fans: draws a fancy opengl sghading of the scan. Does not work
    //! @param if the points of the cloud are not radially ordered
    void draw(bool draw_normals = false,
		      Eigen::Isometry2f T = Eigen::Isometry2f::Identity(),
		      bool draw_pose_origin = false,
		      bool use_fans=false) const;

    //! sets the color of all points in a cloud
    void setColor(const Eigen::Vector3f& color);

    //! binary serialization
    void save(std::ostream& os) const;

    //! binary deserialization
    bool load(std::istream& is);
  };


  //! Guess from the name
  struct Cloud2DWithPose: public Cloud2D{
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    const Eigen::Isometry2f& pose() const {return _pose;}
    void setPose(const Eigen::Isometry2f& pose_) {_pose = pose_;}
    void draw(bool draw_normals = false,
	      bool draw_pose_origin = false,
	      bool use_fans=false) const;
    //! binary serialization
    void save(std::ostream& os) const;
    //! binary deserialization
    bool load(std::istream& is);
    virtual ~Cloud2DWithPose() {}
  protected:
    Eigen::Isometry2f _pose;
  };

}
