#include "projector2d.h"

#include <Eigen/Eigenvalues> 
using namespace std;

namespace srrg_scan_matcher {
  using namespace srrg_core;

  Projector2D::Projector2D() {
    _sct = 0;
    _max_range = 30;
    _min_range = 0.1;
    _num_ranges = 1024;
    setFov(1.5 * M_PI);
    updateParameters();
  }
  
  void Projector2D::updateParameters() {
    _angle_increment = _fov / _num_ranges;

    if (_sct == 0)
      _sct = new SinCosTable(-_fov * 0.5, _angle_increment, _num_ranges);
    else
      *_sct = SinCosTable(-_fov * 0.5, _angle_increment, _num_ranges);
  }

  Projector2D::~Projector2D() {
    if (_sct) 
      delete _sct;
  }

  void Projector2D::project(FloatVector& ranges, IntVector& indices,
			    const Eigen::Isometry2f& T,const Cloud2D& model ) const {
    FloatVector reverse_ranges;
    IntVector reverse_indices;
    project(ranges, indices, reverse_ranges, reverse_indices, T, model);
  }

  void Projector2D::project(FloatVector& ranges, IntVector& indices,
			    FloatVector& reverse_ranges,
			    IntVector& reverse_indices,
			    const Eigen::Isometry2f& T,
			    const Cloud2D& model) const {
    float middle = _num_ranges * 0.5;
    float inverse_angle_increment = 1. / _angle_increment;
    const Eigen::Matrix2f& R = T.linear();
    ranges.resize(_num_ranges);
    indices.resize(_num_ranges);
    std::fill(ranges.begin(), ranges.end(), 1e3);
    std::fill(indices.begin(), indices.end(), -1);
    
    if (reverse_indices.size()) {
      reverse_indices.resize(model.size());
      std::fill(reverse_indices.begin(), reverse_indices.end(), -1);
      reverse_ranges.resize(model.size());
      std::fill(reverse_ranges.begin(), reverse_ranges.end(), 1e3);
    }
    for (size_t i = 0; i < model.size(); ++i){
      const RichPoint2D& rp = model[i];
      Eigen::Vector2f p = T * rp.point();

      float angle = atan2(p.y(), p.x());
      int bin = round(angle * inverse_angle_increment + middle);

      if (bin < 0 || bin >= _num_ranges)
	continue;
      Eigen::Vector2f n = R * rp.normal();
      float range = p.norm();
      float& brange = ranges[bin];

      
      if (brange > range)
	brange = range;
      else 
	continue;

      if (p.dot(n) <= 0) {
	indices[bin] = i;
	if (reverse_indices.size()){
	  reverse_indices [i] = bin;
	  reverse_ranges [i]=range;
	}
      } else
	indices[bin] = -1;
      
    }
  }

 
  void Projector2D::unproject(Cloud2D& model, const FloatVector& ranges) {
    float dist = 0.3*0.3;//0.15*0.15;
    int minPoints = 5;//8;
    Eigen::Vector2f mean;
    Eigen::Matrix2f cov;
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix2f> eigenSolver;

    size_t offset = 0;
    //Cropping ranges out of fov
    //Assume symmetric offset (i.e., -min_angle = max_angle)
    if (ranges.size() > _num_ranges)
      offset = (ranges.size() - _num_ranges)/2;

    model.resize(ranges.size()-2*offset);

    int k = 0;
    for (size_t i = offset; i < ranges.size()-offset; ++i){
      float r = ranges[i];

      if (r != r || r <= _min_range || r >= _max_range)
	continue;

      model[k]._point = _sct->sincos(i-offset) * r;
      model[k]._accumulator = 1.f / r;
      k++;
    }

    model.resize(k);
    
    // extract the normals, naive way
    for (size_t i = 0; i < model.size(); ++i) {
      // for each point go left and right, until the distance
      //from the current point exceeds a threshold, and accumulate the points;
      Eigen::Vector2f p0 = model[i].point();
      Eigen::Vector2f p1 = model[i].point();
      int imin = i, imax = i;

      while (imin > 0){
	if ((p1 - p0).squaredNorm() >= dist) {
	  ++imin;
	  break;
	}

	--imin;
	p1 = model[imin].point();
      }

      p1 = model[i].point();
      while (imax < static_cast<int>(model.size())){
	if((p1 - p0).squaredNorm() >= dist){
	  break;
	}

	++imax;
	p1 = model[imax].point();
      }

      model[i]._normal.setZero();

      if (imax - imin < minPoints) {
	model[i]._normal.setZero();
	continue;
      }
      mean.setZero();
      cov.setZero();

      float scale = 1. / (imax - imin);

      for (int j = imin; j < imax; ++j) {
	mean += model[j].point();
	cov += model[j].point() * model[j].point().transpose();
      }

      mean *= scale;
      cov *= scale;
      cov -= mean * mean.transpose();

      eigenSolver.computeDirect(cov, Eigen::ComputeEigenvectors);
      
      /*if (eigenSolver.eigenvalues()(0) / eigenSolver.eigenvalues()(1) > 1e-1) {
	continue;
	}*/
      Eigen::Vector2f n = eigenSolver.eigenvectors().col(0);

      if (p0.dot(n) > 0)
	n = -n;

      model[i]._normal = n;
    }
    /*
    k =0;
    for (size_t i = 0; i < model.size(); ++i) {
      RichPoint2D& dest = model.at(k);
      RichPoint2D& src = model.at(i);
      if (src.normal().squaredNorm()>0.1) {
    	dest = src;
    	k++;
      }
    }
    model.resize(k);
    */
  }
}
