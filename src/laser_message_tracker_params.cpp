#include "laser_message_tracker_params.h"

namespace srrg_scan_matcher  {
  
LaserMessageTrackerParams::LaserMessageTrackerParams(LaserMessageTracker* tracker_){
  _tracker = tracker_;

  //Default values
  _verbose = false;
  _bpr = 0.4;
  _iterations = 30;
  _inlier_distance = 0.3;
  _min_correspondences_ratio = 0.5;
  _local_map_clipping_range = 5.0;
  _local_map_clipping_translation_threshold = 2.0;
  _voxelize_res = 0.0;
  _merging_distance = 0.2;
  _merging_normal_angle = 1.0;
  _projective_merge = true;
  _dump_filename = std::string("");
  _tracker_damping = 1.0;
  _frame_skip = 1;
  _max_matching_range = 0.0;
  _min_matching_range = 0.0;
  _num_matching_beams = 0;
  _matching_fov = 0.0;
  _odom_weights = Eigen::Vector3i(100,50,100);
  _laser_translation_threshold = 0.1;
  _laser_rotation_threshold = 0.1;
  _correspondence_finder_type = "projective";
}

void LaserMessageTrackerParams::applyParams(){
  if (!_tracker)
    return;
    
  _tracker->setVerbose(_verbose);
  _tracker->setBpr(_bpr);
  _tracker->setIterations(_iterations);
  _tracker->setInlierDistance(_inlier_distance);
  _tracker->setMinCorrespondencesRatio(_min_correspondences_ratio);
  _tracker->setLocalMapClippingRange(_local_map_clipping_range);
  _tracker->setClipTranslationThreshold(_local_map_clipping_translation_threshold);
  _tracker->setVoxelizeResolution(_voxelize_res);
  _tracker->setMergingDistance(_merging_distance);
  _tracker->setMergingNormalAngle(_merging_normal_angle);
  _tracker->setEnableProjectiveMerge(_projective_merge);
  _tracker->setDumpFilename(_dump_filename);
  _tracker->solver()->setDamping(_tracker_damping);

  _tracker->setFrameSkip(_frame_skip);
  
  _tracker->setMaxMatchingRange(_max_matching_range);
  _tracker->setMinMatchingRange(_min_matching_range);
  _tracker->setNumMatchingBeams(_num_matching_beams);
  _tracker->setMatchingFov(_matching_fov);
  _tracker->setOdomWeights(_odom_weights.x(), _odom_weights.y(), _odom_weights.z());
  _tracker->setLaserTranslationThreshold(_laser_translation_threshold);
  _tracker->setLaserRotationThreshold(_laser_rotation_threshold);
  _tracker->setCorrespondenceFinderType(_correspondence_finder_type);

  _tracker->setProjector(NULL); //projector will be initialized with the correct parameters when the next LaserMessage arrives
}

}
