#include "projective_correspondence_finder2d.h"
#include <stdexcept>
#include <GL/gl.h>
namespace srrg_scan_matcher {

  ProjectiveCorrespondenceFinder2D::ProjectiveCorrespondenceFinder2D(Solver2D* solver_, Projector2D* projector_) :
    CorrespondenceFinder2D(solver_){
    _projector=projector_;
    }

  void ProjectiveCorrespondenceFinder2D::init() {
    _correspondences.clear();
    _indices_current.clear();

    if(_projector == NULL || _solver == NULL) {
      throw std::runtime_error("Cannot init correspondences without a projector and a solver");
    }

    _projector->project(
			_projected_reference_ranges,
			_indices_reference,
			Eigen::Isometry2f::Identity(),
			*_solver->reference()
			);
  }

  void ProjectiveCorrespondenceFinder2D::compute() {
    int search_window=5;
    _correspondences.resize(_solver->current()->size()*(2*search_window+1));
    _indices_current.clear();

    if(_projector == NULL || _solver == NULL) {
      throw std::runtime_error("Cannot compute correspondences without a projector and a solver");
    }

    Cloud2D remapped_current=*_solver->current();
    remapped_current.transformInPlace(_solver->T());
    
    _projector->project(
			_projected_current_ranges,
			_indices_current,
			Eigen::Isometry2f::Identity(),
			remapped_current);

    
    size_t size = std::min(_indices_current.size(), _indices_reference.size());
    int k=0;
    for (size_t i = 0; i < size; ++i) {
      int ref_idx = _indices_reference[i];
      if (ref_idx < 0)
	continue;
      const RichPoint2D& pt_ref = (*_solver->reference())[ref_idx];
      for (int j=-search_window; j<=search_window; j+=2){

	int effective_curr_bin=i+j;
	if (effective_curr_bin<0 || effective_curr_bin>=size)
	  continue;

	int curr_idx = _indices_current[effective_curr_bin];
	if (curr_idx < 0)
	  continue;

	const RichPoint2D& pt_curr = remapped_current[curr_idx];
	if((pt_curr.point() - pt_ref.point()).squaredNorm() < _max_squared_distance &&
	   pt_curr.normal().dot(pt_ref.normal()) > _min_normal_cos) {
	  _correspondences[k]=std::make_pair(ref_idx,curr_idx);
	  k++;
	}
      }
    }
    if (k)
      _correspondences.resize(k);
    else
      _correspondences.clear();
  }

}

