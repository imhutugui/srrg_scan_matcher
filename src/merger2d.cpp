#include <stdexcept>
#include "merger2d.h"
#include "cloud_processor.h"

namespace srrg_scan_matcher {
  using namespace std;
  using namespace srrg_core;

  Merger2D::Merger2D(Projector2D* projector_) {
    _projector=projector_;
    _projective_merge_enabled=false;
    _merging_distance=0.5f;
    _merging_normal_angle=1.0f;
  }

  Merger2D::~Merger2D() {
  }

  void Merger2D::compute(Cloud2D& reference, Cloud2D& current, const Eigen::Isometry2f& origin) {

    if (! _projector) {
      throw std::runtime_error("no projector set");
    }

    IntVector reference_indices, current_indices;
    FloatVector reference_ranges, current_ranges;
    _projector->project(reference_ranges, 
			reference_indices, 
			origin, 
			reference);

    _projector->project(current_ranges, current_indices, origin, current);

    CloudProcessor::merge(reference_ranges, reference_indices, reference,
			  current_ranges, current_indices, current,
			  _merging_normal_angle, _merging_distance);

    if (_projective_merge_enabled) {
      IntVector reverse_reference_indices(reference.size());
      FloatVector reverse_reference_ranges(reference.size());
      _projector->project(reference_ranges, 
			  reference_indices, 
			  reverse_reference_ranges, 
			  reverse_reference_indices,
			  origin, 
			  reference);
      
      CloudProcessor::radialMerge(reference_ranges, 
				  reference_indices, 
				  reverse_reference_ranges,
				  reverse_reference_indices,
				  reference, 
				  _merging_normal_angle, // normal threshold in radians
				  _merging_distance);

    }

  }


  void Merger2D::prune(Cloud2D& reference, Cloud2D& current, const Eigen::Isometry2f& origin) {
    
    if (! _projector) {
      throw std::runtime_error("no projector set");
    }

    IntVector reference_indices, current_indices;
    FloatVector reference_ranges, current_ranges;
    _projector->project(reference_ranges, 
			reference_indices, 
			origin, 
			reference);

    _projector->project(current_ranges, current_indices, origin, current);

    CloudProcessor::prune(reference_ranges, reference_indices, reference,
			  current_ranges, current_indices, current,
			  _merging_normal_angle, _merging_distance);

  }
}
