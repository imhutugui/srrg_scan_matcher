#pragma once

#include "cloud2d.h"
#include <fstream>

namespace srrg_scan_matcher {

  /*!
     Processing object that computes how good two clouds align onto each other.
     The two clouds should be in the same reference system

     To use this class you should:
     - set the two clouds (reference and current)
     - call the compute() method, that will process the clouds and set some internal variable
     - see the statistics through accessor methods.

     Example:
     \code{.cpp}
     .....
     Validator2D* v = ....
     ...
     v->setReference(c1);
     v->setCurrent(c2);
     v->compute();
     cerr << " the number of inliers  is " << v->numInliers() << endl;

     \endcode

     The statistics include the usual inliers,outliers,number of correspondences.
     The parameters in the class are used to determine when a point matches.


     THIS IN AN  ABSTRACT BASE CLASS THAT IS SPECIALIZED IN DERIVED ONES
   */
  class Validator2D {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  public:
    Validator2D();
    virtual ~Validator2D();
    virtual void compute()=0;
    inline const Cloud2D* reference() const { return _reference; }
    inline void setReference(Cloud2D* reference_) { _reference = reference_; }
    inline const Cloud2D* current() const { return _current; }
    inline void setCurrent(Cloud2D* current_) { _current = current_; }
    inline int  numInliers() const {return _num_inliers;}
    inline int  numOutliers() const {return _num_outliers;}
    inline int numCorrespondences() const {return _num_correspondences;}
    inline float badPointsRatio() const {return _bad_points_ratio;}
    inline void setInlierDistance(float inlier_distance){_inlier_distance=inlier_distance;}
    inline float inlierDistance()const {return _inlier_distance;}
  protected:
    Cloud2D *_reference, *_current;
    float _bad_points_ratio;
    float _inlier_distance;
    int _num_inliers;
    int _num_outliers;
    int _num_correspondences;
  };
}
