#include "laser_message_tracker.h"

namespace srrg_scan_matcher {

  LaserMessageTracker::LaserMessageTracker(){
    _frame_count = 0;
    _frame_skip = 1;
    _has_guess = false;
    
    _max_matching_range = 0.0;
    _min_matching_range = 0.0;
    _num_matching_beams = 0;
    _matching_fov = 0.0;
    
    _odom_weights << 100, 50, 100;
    
    _laser_translation_threshold = 0.0;
    _laser_rotation_threshold = 0.0;
    
    _projector = NULL;
    
    _previous_odom = Eigen::Isometry2f::Identity();
  }

  void LaserMessageTracker::initializeProjectorFromParameters(LaserMessage* las){
    _projector = new Projector2D;
    
    if (_max_matching_range != 0)
      _projector->setMaxRange(_max_matching_range);
    else
      _projector->setMaxRange(las->maxRange());
  
    if (_min_matching_range != 0)
      _projector->setMinRange(_min_matching_range);
    else
      _projector->setMinRange(las->minRange());
  
    if (_num_matching_beams != 0)
      _projector->setNumRanges(_num_matching_beams);
    else
      _projector->setNumRanges(las->ranges().size());
  
    if (_matching_fov != 0)
      _projector->setFov(_matching_fov);
    else
      _projector->setFov(las->maxAngle()-las->minAngle());
  }

  void LaserMessageTracker::setCorrespondenceFinderType(std::string cfinder){
    //std::cerr << "SETTING correspondence finder type: [" << cfinder << "]" << std::endl;
    
    if (cfinder=="projective"){
      setCorrespondenceFinder(new ProjectiveCorrespondenceFinder2D);
      Tracker2D::setProjector(_projector);
    }
    else if (cfinder=="nn")
      setCorrespondenceFinder(new NNCorrespondenceFinder2D);
    else {
      std::cerr << "unknown correspondence finder type: [" << cfinder << "]" << std::endl;
      exit(0);
    }
  }

  bool LaserMessageTracker::compute(LaserMessage* las){
    if (!las)
      return false;

    std::cerr << "LASER_MESSAGE";
    
    ++_frame_count;
    if (_frame_count % _frame_skip != 0){
      std::cerr << " SKIPPED" << std::endl;
      ++_skipped_frames;
      return false;
    }

    if (_projector == NULL) {
      initializeProjectorFromParameters(las);
      Tracker2D::setProjector(_projector);
    } 

    //Getting laser offset
    Eigen::Isometry2f laser_offset = toIsometry2f(las->offset());
    //Getting current odometry
    Eigen::Isometry2f curr_odom = toIsometry2f(las->odometry());

    //Creating new cloud from laser ranges
    Cloud2D* current = new Cloud2D;
    _projector->unproject(*current, las->ranges());

    if (!_has_guess){
      //First time
      std::cerr << std::endl;
      update(current);
      _has_guess=true;
      _previous_odom=curr_odom;
    } else {
      Eigen::Isometry2f odom_prior_T=_previous_odom.inverse()*curr_odom;
      Eigen::Matrix3f odom_info;
      odom_info.setIdentity();

      Eigen::Vector3f odom_prior=t2v(odom_prior_T);
      odom_info(0,0)= _odom_weights.x()/(1e-3 + fabs(odom_prior.x()));
      odom_info(1,1)= _odom_weights.y()/(1e-3 + fabs(odom_prior.y()));
      odom_info(2,2)= _odom_weights.z()/(1e-3 + fabs(odom_prior.z()));
      
      //skipping lasers if we didn't move enough
      
      if (odom_prior_T.translation().norm() >= _laser_translation_threshold
	  || fabs(odom_prior.z()) >= _laser_rotation_threshold){
	
	std::cerr << std::endl;
	update(current, laser_offset.inverse()*odom_prior_T*laser_offset, odom_info);
	_previous_odom=curr_odom;
      } else {
	std::cerr << " SKIPPED" << std::endl;
        ++_skipped_frames;
	return false;
      }

    }
    return true;
  }
  
}
