#pragma once
#include "correspondence_finder2d.h"
#include "srrg_path_map/distance_map_path_search.h"

namespace srrg_scan_matcher {
  /*!
     Specialization of a correspondence finder that uses a nearest neighbor approach
     relies on a distance map, which is sized and computed after the init
  */

  class NNCorrespondenceFinder2D: public CorrespondenceFinder2D {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    NNCorrespondenceFinder2D(Solver2D* solver = NULL);
    ~NNCorrespondenceFinder2D() {};
    virtual void compute();
    virtual void init();
    inline void setResolution(float res) {_resolution = res; _inverse_resolution = 1./res;}
    inline float resolution() const { return _resolution; }
//private:
    inline Eigen::Vector2i w2m(const Eigen::Vector2f& v) const {
      Eigen::Vector2f mv=(v-_offset)*_inverse_resolution;
      return mv.cast<int>();
    }
    inline bool inside(const Eigen::Vector2i& mp) const { 
      return 
	mp.y()>=0 && 
	mp.y() < _input_index_image.rows &&
	mp.x()>=0 && 
	mp.x() < _input_index_image.cols;
    }
    
    srrg_core::DistanceMapPathSearch _dmap_calculator; // object that computes the index map
    srrg_core::PathMap _distance_map;
    srrg_core::IntImage _input_index_image;
    float _resolution;
    float _inverse_resolution;
    Eigen::Vector2f _offset;
  };
}
