#pragma once
#include "cloud2d.h"
#include "srrg_types/defs.h"
namespace srrg_scan_matcher {
  struct SinCosTable;

  /**
     Projector object used to turn a cloud into
     - an array of ranges (simulated laser)
     - an arrat of indices that for each bin tells which point in the array projects onto that beam

     It can do two things:
     Take a cloud and turn into a scan (projection)
     Take a scan and turn it into a cloud (unprojection);

     Usage:
     \code{.cpp}
     Projector2D * projector=new Projector2D;

     srrg_core::FloatVector ranges;
     srrg_core::FloatVector indices;
     
     projector->project(ranges, indices, my_transform, cloud);
     \endcode}
     
     ranges and indices are of the same size  (number of laser beams) and are populated
     with the ranges of a laser scan observing the cloud from my_transform;

     Occasionally one wants to know which points projects onto which beam
     To this end there is another version of the project
     that provides this information.

     \code{.cpp}
     srrg_core::FloatVector reverse_ranges;
     srrg_core::IntVector reverse_indices;
     projector->project(ranges, indices, reverse_ranges, reverse_indices, my_transform, cloud);
     \endcode}
     
     // reverse_ranges and reverse_indices have the size of rhe cloud and contain the
     // indices of rhe laser beam of each point. -1 if it is out of the scan



     Another use is

     \code{.cpp}
     srrg_core::FloatVector laser_scan = ....
     Cloud2d cloud;
     projector->unproject(cloud, laser_scan);
     \endcode

     cloud is populated based on the laser scan and the normals are computed
     from nearby points.
     
     
   */
  class Projector2D {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    Projector2D();
    ~Projector2D();

    void project(srrg_core::FloatVector& zbuffer, srrg_core::IntVector& indices,
		 const Eigen::Isometry2f& T, 
		 const Cloud2D& model) const;


    void project(srrg_core::FloatVector& zbuffer, srrg_core::IntVector& indices,
		 srrg_core::FloatVector& reverse_ranges, srrg_core::IntVector& reverse_indices,
		 const Eigen::Isometry2f& T, 
		 const Cloud2D& model) const;

    void unproject(Cloud2D& model, const srrg_core::FloatVector& ranges);

    // setter e getter
    inline void setMaxRange(float max_range) { _max_range = max_range; }
    inline float maxRange() const { return _max_range; }
    inline void setMinRange(float min_range) { _min_range = min_range; }
    inline float minRange() const { return _min_range; }
    inline float angleIncrement() const { return _angle_increment; }
    inline void setFov(float fov_) {
      _fov = fov_;
      updateParameters();
    }
    inline float fov() const { return _fov; }
    inline void setNumRanges(int num_ranges) {
      _num_ranges = num_ranges;
      updateParameters();
    }
    inline int numRanges() const { return _num_ranges; }

  protected:
    struct SinCosTable{
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

      SinCosTable(float angle_min, float angle_increment, size_t s){
	init(angle_min, angle_increment, s);
      }

      void init(float angle_min, float angle_increment, size_t s) {
	if (_angle_min == angle_min && _angle_increment == angle_increment && _table.size() == s)
	  return;

	_angle_min = angle_min;
	_angle_increment = angle_increment;
	_table.resize(s);

	for (size_t i = 0; i < s; ++i){
	  float alpha = _angle_min + i * _angle_increment;
	  _table[i] = Eigen::Vector2f(cos(alpha), sin(alpha));
	}
      }

      inline const Eigen::Vector2f& sincos(int i) const {
	return _table[i];
      }

      float _angle_min;
      float _angle_increment;
      srrg_core::Vector2fVector _table;
    };

    void updateParameters();
    float _max_range, _min_range;
    int _num_ranges;
    float _fov;
    float _angle_increment;
    SinCosTable* _sct;
  };

}
