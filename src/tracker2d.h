#pragma once

#include <stdexcept>
#include <iostream>
#include <sys/time.h>

#include "aligner2d.h"
#include "projective_validator2d.h"
#include "merger2d.h"
#include "correspondence_finder2d.h"
#include "nn_correspondence_finder2d.h"

#include "cloud_processor.h"
#include <fstream>

namespace srrg_scan_matcher {

  /*!
     Tracker object;
     it processes sequentially the clouds and updates its internal model
     

     It needs to be initialized according to all its base classes;
     substantially it offers une functionality:
     
     update(new_cloud, guess)
     
     A call to update does the followinf
     - registers the new_cloud to the reference_cloud in the tracker.
       if there is no reference the new cloud is chosen to be the reference

     - it measures the quality of the match (being it a validator it can do that)
       if the match is wrong, 
          it resets the map to the current cloud
       otherwise
          it fuses the new_cloud with the existing map

     - it updates the internal representation and the global position of the robot (globalT)


     To intialize a tracker:

     \code{.cpp}
     Tracker2D tracker=new Tracker2D;
     Projector2D projector* =new Projector2D;
     CorrespondenceFinder2d * cfinder=0;

     if (use_prijectove) { // select your favourite data association
        cfinder=new ProjectiveCorrespondenceFinder2D;
     } else {
        cfinder=new NNCorrespondenceFinder2D;
     }
     tracker->setProjector(new Projector2D);
     \endcode

     To use it

     \code{.cpp}
     while(have_new_cloud) {
       tracker->update(new_cloud);
       
       current_map = tracker->reference(); //this is the current map
       cerr << "robot isin" << globalT();
     }
     \endcode
   */

  class Tracker2D : public Aligner2D, 
		    public ProjectiveValidator2D, 
		    public Merger2D{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    enum Status {OK=0x0, Error=0x1, FirstScan=0x2, BadCorrespondences=0x3, BadInliers=0x4, BadBPR=0x5, SolverNAN=0x6, };

    Tracker2D(Projector2D* projector_=NULL);
    virtual ~Tracker2D();
    void update(Cloud2D* cluoud=0, 
		const Eigen::Isometry2f& prior_mean=Eigen::Isometry2f::Identity(),
		const Eigen::Matrix3f& prior_omega=Eigen::Matrix3f::Zero());
    void reset(const Eigen::Isometry2f& global_t_=Eigen::Isometry2f::Identity());

    inline Status status() const {return _status;}

    //inline void setIterations(const int &iterations) { _iterations = iterations; }
    inline void setBpr(const float &bpr) { _bpr = bpr; }
    inline void setGlobalT(const Eigen::Isometry2f& global_t) {_global_t = global_t;}
    inline Eigen::Isometry2f globalT() { return _global_t; }

    inline const Cloud2D* reference() const { return _reference; }
    inline Cloud2D* reference() { return _reference;}

    inline void setReference(Cloud2D* reference_) { 
      _reference=reference_;
      Aligner2D::setReference(reference_); 
      ProjectiveValidator2D::setReference(reference_);
    }

    inline const Cloud2D* current() const { return _current; }
    inline Cloud2D* current() { return _current;}

    inline void setCurrent(Cloud2D* current_) {
      _current=current_;
      Aligner2D::setCurrent(current_); 
      ProjectiveValidator2D::setCurrent(current_);
    }

    inline Projector2D* projector() { return ProjectiveValidator2D::projector();}
    inline const Projector2D* projector() const { return ProjectiveValidator2D::projector(); }
    void setProjector(Projector2D* projector);

    inline void setMinCorrespondencesRatio(float ratio){ _min_correspondences_ratio = ratio; }
    inline float minCorrespondencesRatio() const { return _min_correspondences_ratio; }

    inline float localMapClippingRange() const { return _local_map_clipping_range; }
    inline float clipTranslationThreshold() const { return _clip_translation_threshold; }

    inline void setLocalMapClippingRange(float range) { _local_map_clipping_range = range; }
    inline void setClipTranslationThreshold(float t) { _clip_translation_threshold = t; }
    
    inline void setVoxelizeResolution(float vres) {_voxelize_resolution=vres;}
    inline float voxelizeResolution() const  {return _voxelize_resolution;}
    inline float cycleTime() const {return _cycle_time;}

    inline void setVerbose(bool verbose) {_verbose=verbose;}


    std::string dumpFilename() const {return _dump_filename;}
    void setDumpFilename(const std::string& dump_filename_);
    void dump();
    void clear();
    
  private:
    Cloud2D* _current, *_reference;
   
    int _iterations;			       
    float _bpr;
    float _min_correspondences_ratio;
    float _cycle_time;    
    Eigen::Isometry2f _global_t;
    Eigen::Isometry2f _last_clipped_pose;
    float _clip_translation_threshold;
    float _local_map_clipping_range;
    float _voxelize_resolution;
    
    std::ofstream* _dump_stream;
    std::string _dump_filename;
    int _frame_count;

    bool _verbose;
    Status _status;
  };
}
